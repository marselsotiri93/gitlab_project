#!/bin/bash

# Define Bitbucket and GitLab repository URLs.
BITBUCKET_REPO_URL="https://github.com/marselsotiri/test1.git"
GITLAB_REPO_URL="https://gitlab.com/devops2742543/gitlab_project.git"

# Clone Bitbucket repository.
git clone --mirror "$BITBUCKET_REPO_URL" bitbucket_mirror.git

# Push to GitLab.
cd bitbucket_mirror.git
git push --mirror "$GITLAB_REPO_URL"

