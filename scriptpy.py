import subprocess

def check_for_changes(source_repo_path, target_repo_path):
    # Step 1: Check for changes in the source repository
    source_has_changes = check_repository_for_changes(source_repo_path)

    if source_has_changes:
        # Step 2: Fetch and merge or rebase the changes in the source repository
        fetch_and_merge_changes(source_repo_path)

        # Step 3: Push the changes to the target repository if necessary
        push_changes_to_target(source_repo_path, target_repo_path)
    else:
        print("No changes detected in the source repository.")

def check_repository_for_changes(repo_path):
    try:
        # Check if there are changes in the source repository
        output = subprocess.check_output(['git', '-C', repo_path, 'status', '--porcelain'])
        return len(output.strip()) > 0
    except subprocess.CalledProcessError:
        return False

def fetch_and_merge_changes(repo_path):
    # Fetch changes from the remote repository
    subprocess.call(['git', '-C', repo_path, 'fetch'])

    # Decide whether to merge or rebase based on your workflow
    # Merge:
    # subprocess.call(['git', '-C', repo_path, 'merge', 'origin/master'])
    
    # Rebase:
    # subprocess.call(['git', '-C', repo_path, 'rebase', 'origin/master'])

def push_changes_to_target(source_repo_path, target_repo_path):
    # Push the changes from the source repository to the target repository
    subprocess.call(['git', '-C', source_repo_path, 'push', target_repo_path])

if __name__ == "__main__":
    source_repo_path = 'https://marselsotiri@bitbucket.org/devopstiranal/sda.git'
    target_repo_path = 'https://gitlab.com/devops2742543/gitlab_project.git'
    
    check_for_changes(source_repo_path, target_repo_path)