#!/bin/bash

source_repo="/home/marsel/Desktop/test1"
target_repo="/home/marsel/Desktop/Sdapractice/gitlab_project"

git -C /home/marsel/Desktop/test1 fetch
git -C /home/marsel/Desktop/Sdapractice/gitlab_project fetch

if [ "$(git -C "$source_repo" rev-parse HEAD)" != "$(git -C "$target_repo" rev-parse HEAD)" ]; then
    # If they have diverged, perform a pull (or merge) in the target repository
    git -C "$source_repo" rebase
fi


